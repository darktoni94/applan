﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class random_planGenerator : MonoBehaviour {

    public Text inputText;
    int rand;
    int previousRand;
    public string[] myString;

    void Start()
    {
        printPlan();
    }

    public void printPlan()
    {
        rand = Random.Range(0, 20);
        if(rand == previousRand && previousRand != 20){
            rand++;
        }else if(rand == previousRand && previousRand != 0)
        {
            rand--;
        }

        inputText.text = myString[rand];

        myString[rand] = myString[rand].Replace("AAA", "\n");

        previousRand = rand;
    }
}
